package util;

import appointment.Appointment;
import client.Client;
import haidresser.Hairdresser;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.function.Consumer;
import java.util.function.Function;

public class SessionManagerTest extends AbstractSessionManager{

    private static final SessionManager INSTANCE = new SessionManager();

    private SessionManagerTest() {
    }

    public static SessionFactory getSessionFactory() {
        return INSTANCE.getSessionFactory("practical_project_test");
    }

    public static void shutdown() {
        INSTANCE.shutdownSessionManager();
    }

    @Override
    protected void setAnnotatedClasses(Configuration configuration) {
        configuration.addAnnotatedClass(Client.class);
        configuration.addAnnotatedClass(Hairdresser.class);
        configuration.addAnnotatedClass(Appointment.class);

    }

    public static void runInTransaction(Consumer<Session> consumer) {
        runInTransaction(session ->
        {
            consumer.accept(session);
            return null;
        });
    }

    public static <T> T runInTransaction(Function<Session, T> consumer) {
        Transaction transaction = null;
        try {
            Session session = getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();

            T result = consumer.apply(session);

            transaction.commit();
            return result;
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new RuntimeException(ex);
        }
    }

    public static Session getSession() {
        if(SessionManager.getSessionFactory().isOpen()) {
            return SessionManager.getSessionFactory().getCurrentSession();
        }

        return SessionManager.getSessionFactory().openSession();
    }
}
