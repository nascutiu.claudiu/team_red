package util;

import appointment.Appointment;
import appointment.AppointmentControler;
import appointment.AppointmentControlerTest;
import client.Client;
import client.ClientControler;
import client.ClientControlerTest;
import haidresser.Hairdresser;
import haidresser.HairdresserControler;
import hairdresser.HairdresserControlerTest;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;
import java.util.Scanner;

public class ServiceTest {

    Scanner scanner = new Scanner(System.in);
    Scanner scannerText = new Scanner(System.in);
    Scanner input = new Scanner(System.in);

    private ClientControlerTest clientControler = new ClientControlerTest();
    private HairdresserControlerTest hairdresserControler = new HairdresserControlerTest();
    private final AppointmentControlerTest appointmentControler = new AppointmentControlerTest();

    @Test
    public void addClientTest() {
        //given
        Client client = new Client("a", "b", "0",1, Genre.M,"@", "p");

        //when
        SessionManagerTest.runInTransaction(session -> {
            session.save(client);
        });

        //then
        SessionManagerTest.runInTransaction(session -> {
            session.find(Client.class, client);
        });

    }


    public void addHairdresser() {
    }

    public void addApointment() {
    }


    public void editAppointment() {
    }

    public void deleteAppointment() {

    }

}
