package hairdresser;

import haidresser.Hairdresser;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.junit.jupiter.api.Test;
import util.JobType;
import util.SessionManager;

import java.util.List;
import java.util.Scanner;

public class HairdresserControlerTest {

    Scanner scanner = new Scanner(System.in);

    public Hairdresser addHairdresser() {

        System.out.println("Enter the hairdresser firstname:");
        String firstName = scanner.nextLine();
        System.out.println("Enter lastname:");
        String lastName = scanner.nextLine();
        System.out.println("Enter phone number:");
        String phoneNumber = scanner.nextLine();
        System.out.println("Part time = part, Full time = full");
        String jobType = scanner.nextLine();
        JobType inputJobType = jobType.equalsIgnoreCase("part") ? JobType.PARTTIME : JobType.FULLTIME;
        return new Hairdresser(firstName, lastName, phoneNumber, inputJobType);
    }

    public List<Hairdresser> getAllHairdresser() {
        Session session = SessionManager.getSessionFactory().openSession();
        String query = "from Hairdresser";
        Query<Hairdresser> query1 = session.createQuery(query);
        List<Hairdresser> hairdressers = query1.list();

        session.close();
        return hairdressers;
    }

}
