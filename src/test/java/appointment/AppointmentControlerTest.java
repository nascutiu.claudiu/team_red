package appointment;

import client.Client;
import haidresser.Hairdresser;
import org.hibernate.Session;
import org.hibernate.query.Query;
import util.SessionManager;

import java.util.List;
import java.util.Scanner;

public class AppointmentControlerTest {

    private Scanner scannerNumber;
    private Scanner scanner;

    public AppointmentControlerTest() {
        this.scanner = new Scanner(System.in);
        this.scannerNumber = new Scanner(System.in);
    }

    public Appointment addAppoiment(List<Client> clientList, List<Hairdresser> hairdresserList) {

        for (int i = 0; i < clientList.size(); i++) {
            System.out.println(i + " " + clientList.get(i).toString());
        }
        System.out.println("Selectati clientul:");
        int selectedClientPosition = scannerNumber.nextInt();

        for (int i = 0; i < hairdresserList.size(); i++) {
            System.out.println(i + " " + hairdresserList.get(i).toString());
        }
        System.out.println("Selectati frizerul dorit:");
        int selectedHairdresserPosition = scannerNumber.nextInt();
        System.out.println("Introduceti data: ");
        String date = scanner.nextLine();
        Appointment appointment = new Appointment(date, clientList.get(selectedClientPosition),
                hairdresserList.get(selectedHairdresserPosition));
        return appointment;
    }

    public void showClientAppointments(Client client){

        List<Appointment> appointmentList = getAppointmentListByClient(client.getId());
        System.out.println("----------");
        System.out.println(appointmentList);
    }

    public List<Appointment> getAppointmentListByClient(int clientId) {
        Session session = SessionManager.getSessionFactory().openSession();
        String query = "from Appointment a where a.client = " + clientId;
        Query<Appointment> query1 = session.createQuery(query);
        List<Appointment> appointments = query1.list();

        session.close();
        return appointments;
    }

    public List<Appointment> getAllAppointments(){
        Session session = SessionManager.getSessionFactory().openSession();
        String query = "from Appointment";
        Query<Appointment> query1 = session.createQuery(query);
        List<Appointment> appointments = query1.list();

        session.close();
        return appointments;
    }

}
