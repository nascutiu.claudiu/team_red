package client;

import org.hibernate.Session;
import org.hibernate.query.Query;
import util.Genre;
import util.SessionManager;

import java.util.List;
import java.util.Scanner;

public class ClientControlerTest {

    Scanner scanner = new Scanner(System.in);
    Scanner scannerNumber = new Scanner(System.in);

    public Client inputClientData() {
        System.out.println("Enter your firstname:");
        String firstName = scanner.nextLine();
        System.out.println("Enter your lastname:");
        String lastName = scanner.nextLine();
        System.out.println("Enter your phone number: ");
        String phoneNumber = scanner.nextLine();
        System.out.println("Enter your age:");
        Integer age = scannerNumber.nextInt();
        System.out.println("M = M, F = F");
        String genre = scanner.nextLine();
        Genre inputGenre = genre.equalsIgnoreCase("M") ? Genre.M : Genre.F;
        System.out.println("Enter your email:");
        String email = scanner.nextLine();
        System.out.println("Enter your password:");
        String password = scanner.nextLine();
        return new Client(firstName, lastName, phoneNumber, age, inputGenre, email, password);
    }

    public List<Client> getAllClients() {
        Session session = SessionManager.getSessionFactory().openSession();
        String query = "from Client";
        Query<Client> query1 = session.createQuery(query);
        List<Client> clients = query1.list();

        session.close();
        return clients;
    }

    public boolean isExistingEmail(String email) {
        Session session = SessionManager.getSessionFactory().openSession();
        String client1 = "";
        try {
            Query<Client> query1 = session.createQuery("select email from Client where email = :email");
            query1.setParameter("email", email);
            client1 = String.valueOf(query1.getSingleResult());
        } catch (Exception e) {
            System.out.println("Your email or password is wrong");
        }
        if (client1.equals(email)) {
            System.out.println();
            return true;
        }
        session.close();
        return false;
    }

}
