package client;

import appointment.Appointment;
import util.Genre;

import javax.persistence.*;

@Entity
public class Client extends Person {

    @Column
    private Integer age;

    @Column
    @Enumerated(EnumType.STRING)
    private Genre genre;

    @Column
    private String email;

    private String password;

    @OneToOne
    @JoinColumn(name = "appointment_id")
    private Appointment appointment;

    public Client() {
    }

    public Client(String firstName, String lastName, String phoneNumber, Integer varsta, Genre genre,
                  String email, String password) {
        super(firstName, lastName, phoneNumber);
        this.age = varsta;
        this.genre = genre;
        this.email = email;
        this.password = password;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    @Override
    public String toString() {
        return super.toString() +
                "varsta=" + age +
                ", genre=" + genre +
                ", email = " + email +
                '}';
    }
}
