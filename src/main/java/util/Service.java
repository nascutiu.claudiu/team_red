package util;

import appointment.Appointment;
import appointment.AppointmentControler;
import client.Client;
import client.ClientControler;
import haidresser.Hairdresser;
import haidresser.HairdresserControler;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;
import java.util.Scanner;

public class Service {

    Scanner scanner = new Scanner(System.in);
    Scanner scannerText = new Scanner(System.in);
    Scanner input = new Scanner(System.in);

    private ClientControler clientControler = new ClientControler();
    private HairdresserControler hairdresserControler = new HairdresserControler();
    private final AppointmentControler appointmentControler = new AppointmentControler();
    private final Backup backup = new Backup();

    public void addClient() {
        SessionManager.runInTransaction(session -> {
            session.save(clientControler.inputClientData());

            session.flush();
            session.clear();

        });
    }

    public void addHairdresser() {
        SessionManager.runInTransaction(session -> {
            session.save(hairdresserControler.addHairdresser());

            session.flush();
            session.clear();


        });
    }

    public void addApointment() {
        List<Client> clientList = this.clientControler.getAllClients();
        List<Hairdresser> hairdresserList = this.hairdresserControler.getAllHairdresser();
        SessionManager.runInTransaction(session -> {
            session.save(appointmentControler.addAppoiment(clientList, hairdresserList));

            session.flush();
            session.clear();
        });
    }

    public void showClientAppointment() {
        List<Client> clients = clientControler.getAllClients();
        for (int i = 0; i < clients.size(); i++) {
            System.out.println(i + " " + clients.get(i).toString());
        }
        int selectedClient = scanner.nextInt();
        appointmentControler.showClientAppointments(clients.get(selectedClient));
    }

    public void editAppointment() {
        List<Client> clients = clientControler.getAllClients();
        for (int i = 0; i < clients.size(); i++) {
            System.out.println(i + " " + clients.get(i).toString());
        }
        System.out.println("Selectati clientul:");
        int selectedClient = scanner.nextInt();

        List<Appointment> appointmentList = appointmentControler
                .getAppointmentListByClient(clients.get(selectedClient).getId());

        for (int i = 0; i < appointmentList.size(); i++) {
            System.out.println(i + " " + appointmentList.get(i).toString());
        }

//        appointmentList.forEach((Appointment appointment) -> System.out.println(appointment.toString()));
        System.out.println("Selectati programarea:");
        int selectedAppointment = scanner.nextInt();

        Appointment appointment = appointmentList.get(selectedAppointment);

        System.out.println("Introduceti data noii programari (zz.mm.yy):");
        String newDate = scannerText.nextLine();
        appointment.setDate(newDate);

        SessionManager.runInTransaction(session -> {
            session.update(appointment);
        });

        System.out.println("Data a fost modificata!");
    }

    public void deleteAppointment() {
        List<Client> clients = clientControler.getAllClients();
        for (int i = 0; i < clients.size(); i++) {
            System.out.println(i + " " + clients.get(i).toString());
        }
        System.out.println("Selectati clientul:");
        int selectedClient = scanner.nextInt();

        List<Appointment> appointmentList = appointmentControler
                .getAppointmentListByClient(clients.get(selectedClient).getId());

        for (int i = 0; i < appointmentList.size(); i++) {
            System.out.println(i + " " + appointmentList.get(i).toString());
        }

//        appointmentList.forEach((Appointment appointment) -> System.out.println(appointment.toString()));
        System.out.println("Selectati programarea pe care doriti sa o stergeti:");
        int selectedAppointment = scanner.nextInt();

        Appointment appointment = appointmentList.get(selectedAppointment);

        SessionManager.runInTransaction(session -> {
            session.delete(appointment);
        });
    }

    public void signUp() {
        int selectedOption;
        String userAnswer;
        do {
            System.out.println("1. Hairdresser");
            System.out.println("2. Client");
            System.out.println("Choose a valide option:");
            selectedOption = scanner.nextInt();
            switch (selectedOption) {
                case 1:
                    addHairdresser();
                    break;
                case 2:
                    addClient();
                    break;
                default:
                    System.out.println("Please choose a valide option!");
            }
            do {
                System.out.println("Thank you for sign up!");
                System.out.println("Do you want to log in? (Y/N)");
                userAnswer = scannerText.nextLine();
                if (!userAnswer.equalsIgnoreCase("N") && !userAnswer.equalsIgnoreCase("Y")) {
                    System.out.println("Enter either Y or N!");
                }
                if (userAnswer.equalsIgnoreCase("Y")) {
                    login();
                } else {
                    exit();
                    return;
                }
            } while (!userAnswer.equalsIgnoreCase("N") && !userAnswer.equalsIgnoreCase("Y"));
        } while (selectedOption != 2 || userAnswer.equalsIgnoreCase("Y"));
    }

    public void login() {
        System.out.println("Enter your email:");
        String userEmail = scannerText.nextLine();
        System.out.println("Enter your password:");
        String userPassword = scannerText.nextLine();
        if (clientControler.isExistingEmail(userEmail)) {
            loginMenu();
        } else {
            System.out.println("Please enter a valide email or password!");
            login();
        }
    }

    private void loginMenu() {
        int selectedOption;
        String userAnswer = "";
        do {
            showMenuForLogin();
            System.out.println("Choose a valide option:");
            selectedOption = scanner.nextInt();
            switch (selectedOption) {
                case 1:
                    addApointment();
                    break;
                case 2:
                    showClientAppointment();
                    break;
                case 3:
                    editAppointment();
                    break;
                case 4:
                    deleteAppointment();
                    break;
                case 5:
                    backup();
                    break;
                case 6:
                    startApp();
                default:
                    System.out.println("Please choose a valide option!");
            }
            if (selectedOption != 6) {
                do {
                    System.out.println("Do you want to choose another option? (Y/N)");
                    userAnswer = scannerText.nextLine();
                    if (!userAnswer.equalsIgnoreCase("N") && !userAnswer.equalsIgnoreCase("Y")) {
                        System.out.println("Enter either Y or N!");
                    }
                    if (userAnswer.equalsIgnoreCase("N")) {
                        exit();
                        return;
                    }
                } while (!userAnswer.equalsIgnoreCase("N") && !userAnswer.equalsIgnoreCase("Y"));
            }
        } while (selectedOption != 6 || userAnswer.equalsIgnoreCase("Y"));
    }


    public void startApp() {
        int selectedOption;
        String userAnswer = "";
        do {
            showMenu();
            System.out.println("Choose a valide option:");
            selectedOption = scanner.nextInt();
            switch (selectedOption) {
                case 1:
                    signUp();
                    break;
                case 2:
                    login();
                    break;
                case 3:
                    exit();
                    break;
                default:
                    System.out.println("Please choose a valide option");
            }
            if (selectedOption != 3) {
                do {
                    System.out.println("Do you want to choose another option? (Y/N)");
                    userAnswer = scannerText.nextLine();
                    if (!userAnswer.equalsIgnoreCase("N") && !userAnswer.equalsIgnoreCase("Y")) {
                        System.out.println("Enter either Y or N!");
                    }
                    if (userAnswer.equalsIgnoreCase("N")) {
                        exit();
                        return;
                    }
                } while (!userAnswer.equalsIgnoreCase("N") && !userAnswer.equalsIgnoreCase("Y"));
            }

        } while (selectedOption != 3 || userAnswer.equalsIgnoreCase("Y"));

    }

    private void backup() {
        int selectedOption;
        String userAnswer = "";
        do {
            showBackupMenu();
            System.out.println("Choose who you want to backup: ");
            selectedOption = scanner.nextInt();
            switch (selectedOption) {
                case 1:
                    backup.addClients(clientControler.getAllClients());
                    break;
                case 2:
                    backup.addHairdresser(hairdresserControler.getAllHairdresser());
                    break;
                case 3:
                    backup.addAppointment(appointmentControler.getAllAppointments());
                    break;
                default:
                    System.out.println("Please choose a valide option!");
            }
            if (selectedOption != 3) {
                do {
                    System.out.println("Do you want to choose another option? (Y/N)");
                    userAnswer = scannerText.nextLine();
                    if (!userAnswer.equalsIgnoreCase("N") && !userAnswer.equalsIgnoreCase("Y")) {
                        System.out.println("Enter either Y or N!");
                    }
                    if (userAnswer.equalsIgnoreCase("N")) {
                        exit();
                        return;
                    }
                } while (!userAnswer.equalsIgnoreCase("N") && !userAnswer.equalsIgnoreCase("Y"));
            }
        } while (selectedOption != 3 || userAnswer.equalsIgnoreCase("Y"));
    }

    public void showBackupMenu() {
        System.out.println("=== Backup Menu ===");
        System.out.println("1. Backup for client");
        System.out.println("2. Backup for hairdresser");
        System.out.println("3. Backup for appointment");
    }

    public void showCalendar(int month, int year) {
        YearMonth ym = YearMonth.of(year, month);
        System.out.println("Sun Mon Tue Wed Thu Fri Sat");
        int counter = 1;

        int dayValue = LocalDate.of(year, month, 1).getDayOfWeek().getValue();
        if (dayValue != 7)
            for (int i = 0; i < dayValue; i++, counter++) {
                System.out.printf("%-4s", "");
            }

        for (int i = 1; i <= ym.getMonth().length(ym.isLeapYear()); i++, counter++) {
            System.out.printf("%-4d", i);

            if (counter % 7 == 0) {
                System.out.println();
            }
        }
        System.out.println();
    }

    public void showMenu() {
        System.out.println("1. Sign up");
        System.out.println("2. Log in");
        System.out.println("3. Exit");
    }

    public void showMenuForLogin() {
        System.out.println("1. Make an appointment!");
        System.out.println("2. Show your appointment!");
        System.out.println("3. Edit your Appointment");
        System.out.println("4. Delete your appointment!");
        System.out.println("5. Backup");
        System.out.println("6. Log out");
    }

    public void exit() {
        System.out.println("Thank you!");
        SessionManager.shutdown();
    }

    public ClientControler getClientControler() {
        return clientControler;
    }

    public void setClientControler(ClientControler clientControler) {
        this.clientControler = clientControler;
    }

    public HairdresserControler getHairdresserControler() {
        return hairdresserControler;
    }

    public void setHairdresserControler(HairdresserControler hairdresserControler) {
        this.hairdresserControler = hairdresserControler;
    }
}
