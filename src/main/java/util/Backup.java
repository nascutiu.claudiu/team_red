package util;

import appointment.Appointment;
import client.Client;
import haidresser.Hairdresser;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class Backup {
    private static final String COMMA = ",";

    public void addClients(List<Client> client) {
        try {
            Files.writeString(Path.of("src/main/resources/client.txt"), parseFileLine(client).toString(),
                    StandardOpenOption.APPEND);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private StringBuilder parseFileLine(List<Client> client) {
        StringBuilder contentStr = new StringBuilder();

        for (Client value : client) {
            contentStr.append(value.getId());
            contentStr.append(COMMA);
            contentStr.append(value.getFirstName());
            contentStr.append(COMMA);
            contentStr.append(value.getLastName());
            contentStr.append(COMMA);
            contentStr.append(value.getPhoneNumber());
            contentStr.append(COMMA);
            contentStr.append(value.getAge());
            contentStr.append(COMMA);
            contentStr.append(value.getGenre());
            contentStr.append("\n");
        }
        return contentStr;
    }


    public void addHairdresser(List<Hairdresser> hairdresser) {
        try {
            Files.writeString(Path.of("src/main/resources/hairdresser.txt"), parseFileLineHairdresser(hairdresser),
                    StandardOpenOption.APPEND);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private StringBuilder parseFileLineHairdresser(List<Hairdresser> hairdressers) {
        StringBuilder content = new StringBuilder();

        for (Hairdresser value : hairdressers) {
            content.append(value.getId());
            content.append(COMMA);
            content.append(value.getFirstName());
            content.append(COMMA);
            content.append(value.getLastName());
            content.append(COMMA);
            content.append(value.getPhoneNumber());
            content.append(COMMA);
            content.append(value.getJobType());
            content.append("\n");
        }
        return content;
    }

    public void addAppointment(List<Appointment> appointments){
        try{
            Files.writeString(Path.of("src/main/resources/appointment.txt"), parseFileLineAppointment(appointments),
                    StandardOpenOption.APPEND);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private StringBuilder parseFileLineAppointment(List<Appointment> appointments){
        StringBuilder content = new StringBuilder();

        for(Appointment value: appointments){
            content.append(value.getId());
            content.append(COMMA);
            content.append(value.getDate());
            content.append(COMMA);
            content.append(value.getClient());
            content.append(COMMA);
            content.append(value.getHairdresser());
            content.append("\n");
        }
        return content;
    }
}
