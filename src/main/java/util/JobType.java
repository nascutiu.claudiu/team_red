package util;

public enum JobType {
    PARTTIME, FULLTIME;
}
