package haidresser;

import client.Person;
import util.JobType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class Hairdresser extends Person {

    @Column
    @Enumerated(EnumType.STRING)
    private JobType jobType;

    public Hairdresser() {
    }

    public Hairdresser(String firstName, String lastName, String phoneNumber, JobType jobType) {
        super(firstName, lastName, phoneNumber);
        this.jobType = jobType;
    }

    public JobType getJobType() {
        return jobType;
    }

    public void setJobType(JobType jobType) {
        this.jobType = jobType;
    }

    @Override
    public String toString() {
        return super.toString() +
                "jobType=" + jobType +
                '}';
    }
}
